import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArambHomeComponent } from './aramb-home.component';

describe('ArambHomeComponent', () => {
  let component: ArambHomeComponent;
  let fixture: ComponentFixture<ArambHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArambHomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArambHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
