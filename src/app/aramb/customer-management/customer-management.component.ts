import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
declare var $: any;
@Component({
  selector: 'app-customer-management',
  templateUrl: './customer-management.component.html',
  styleUrls: ['./customer-management.component.scss']
})
export class CustomerManagementComponent implements OnInit {

  constructor(private siteService: SiteService, private router: Router,) { }


  /////////////////////////////////////////////////////////////////////////////////////////////
  // DataTable Imports // Before Construtor
  @ViewChild(DataTableDirective, { static: false })
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};
  /////////////////////////////////////////////////////////////////////////////////////////////

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  Factors: any = [];
  list_factor() {
    this.siteService.visitor_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.rerender()

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Create Category [ visitor ]
  Factor: any = new Object()
  create_factor(form: NgForm) {
    this.Factor.email = form.value.customer_mobile
    this.Factor.password = form.value.customer_mobile

    // console.log(this.Factor);
    this.siteService.visitor_create(this.Factor).subscribe(
      resp => {
        this.Factor.visitor_id = resp.visitor_id
        console.log(this.Factor);

        console.log(resp.customer_id)
        this.siteService.visitor_edit(this.Factor).subscribe(
          resp => {
            console.log(resp);

            this.list_factor()
            this.Factor = new Object()
            $('#create-factor-modal').modal('hide');
            form.reset()

          }
        )
      },
      error => {
        console.log('error', error);
      }
    );
  }
  // End Create Category
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category [ visitor ]
  edit_factor(form: NgForm) {

    this.siteService.visitor_edit(this.Factor).subscribe(
      resp => {
        console.log("this.Factor");
        console.log(resp)
        this.list_factor()
        this.Factor = new Object()
        $('#edit-factor-modal').modal('hide');
        form.reset()
      },
      error => {
        console.log('error', error);
      }
    );
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  // Start Delete Factor [ visitor -  ]
  delete_factor(ele: any) {
    this.Factor.visitor_id = ele.visitor_id
    this.siteService.visitor_delete(this.Factor).subscribe(
      resp => {
        this.list_factor()
        $('#edit-factor-modal').modal('hide');

      },
    )
  }
  // End Delete Factor
  /////////////////////////////////////////////////////////////////////////////////////////////

  show_edit(element: any) {
    console.log(element);
    this.Factor = element
    $('#edit-factor-modal').modal('show')
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  show_send_whatsapp(factor) {
    this.Factor = factor
    $('#whatsapp-factor-modal').modal('show')
  }

  send_whatsapp(form: NgForm) {
    let url = "https://api.whatsapp.com/send/?phone=91" + form.value.customer_mobile + "&text=" + form.value.wp_message
    window.open(url, "_blank");
    $('#whatsapp-factor-modal').modal('hide')
    form.reset()
  }
  /////////////////////////////////////////////////////////////////////////////////////////////





  /////////////////////////////////////////////////////////////////////////////////////////////
  // DataTables Functions // Functions at last
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }


  // Put this funtion inside the table fetcher
  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}
