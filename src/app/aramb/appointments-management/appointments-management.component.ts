import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { SiteService } from './../../backend/site.service';
import { Subject } from 'rxjs';
import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-appointments-management',
  templateUrl: './appointments-management.component.html',
  styleUrls: ['./appointments-management.component.scss']
})
export class AppointmentsManagementComponent implements OnInit {
  constructor(private siteService: SiteService, private router: Router) { }

  dtTrigger: Subject<any> = new Subject<any>();
  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {

    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////

  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message: any = new Object();
  Factors: any = [];
  list_factor() {
    this.siteService.post_list().subscribe(
      resp => {
        this.Factors = resp
        console.log(resp);
        this.dtTrigger.next();

      })
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  Factor: any = new Object()
  delete_factor(element: any) {

    this.Factor.product_id = element.product_id
    this.siteService.category_delete(this.Factor).subscribe(
      resp => {
        const location = window.location.pathname
        this.router.navigateByUrl('/dashboard', { skipLocationChange: true }).then(() => {
          this.router.navigate([location]);
        });
      },
    )
  }
  /////////////////////////////////////////////////////////////////////////////////////////////


  /////////////////////////////////////////////////////////////////////////////////////////////
  selectedImage: File = null;
  onImage(event) {
    this.selectedImage = <File>event.target.files[0];
  }

  /////////////////////////////////////////////////////////////////////////////////////////////
  // Update Category
  edit_factor(form: NgForm) {
    const uploadData = new FormData();
    uploadData.append('category_id', form.value.category_id)
    uploadData.append('category_name', form.value.category_name)
    if (this.selectedImage != null) {
      uploadData.append('category_cover', this.selectedImage, this.selectedImage.name)
    }

    this.siteService.category_edit(uploadData).subscribe(
      resp => {
        console.log(resp)
        // this.router.navigate(['/dashboard/cat-management'])
      },
      error => {
        console.log('error', error);
      }
    );
  }

  show_edit(element: any) {
    console.log(element);
    $('#edit-factor-modal').modal('show')
    // var edit_modal = document.getElementById('edit-factor-modal')
    // edit_modal.modal(show)

  }
  /////////////////////////////////////////////////////////////////////////////////////////////



}
