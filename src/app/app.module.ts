import { AuthGuard } from './backend/auth.guard';
import { CommonService } from './backend/common.service';
import { SiteService } from './backend/site.service';
import { InterceptorService } from './backend/interceptor.service';
import { LoginComponent } from './auth/login/login.component';
import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DataTablesModule } from 'angular-datatables';
import { ToastrModule } from 'ngx-toastr';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CategoryManagementComponent } from './aramb/category-management/category-management.component';
import { RequestComponent } from './dashboard/request/request.component';

// For Google OAuth
import { SubCategoryManagementComponent } from './aramb/sub-category-management/sub-category-management.component';
import { GalleryManagementComponent } from './aramb/gallery-management/gallery-management.component';
import { AppointmentsManagementComponent } from './aramb/appointments-management/appointments-management.component';
import { CustomerManagementComponent } from './aramb/customer-management/customer-management.component';
import { SiteManagementComponent } from './aramb/site-management/site-management.component';
import { ArambHomeComponent } from './aramb/aramb-home/aramb-home.component';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatInputModule } from '@angular/material/input';


// Material Imports
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [
    AppComponent,
    DashboardHomeComponent,
    DashboardNavComponent,
    CategoryManagementComponent,
    RequestComponent,
    LoginComponent,
    SubCategoryManagementComponent,
    GalleryManagementComponent,
    AppointmentsManagementComponent,
    CustomerManagementComponent,
    SiteManagementComponent,
    ArambHomeComponent,
  ],
  imports: [
    // Basic imports for all ithalam kandipa irukanum
    // npm install -D @angular/cdk ngx-toastr bootstrap angularx-social-login jquery popper.js chart.js font-awesome hammerjs animate.css datatables.net angular-bootstrap-md datatables.net-dt angular-datatables @types/jquery @types/datatables.net --save
    BrowserModule,
    BrowserAnimationsModule, // Animations Uhhh
    ToastrModule.forRoot(), // for pop up notification
    HttpClientModule, // to fetch with backend
    CommonModule,
    FormsModule, // Normal Form
    // ReactiveFormsModule, // for image forms
    DataTablesModule, // All tables
    MDBBootstrapModule.forRoot(), // Advanced Bootstrap
    AppRoutingModule, // app.routing
    // npm i @ng-select / ng - select
    NgSelectModule,


    // Material Components
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatCardModule,
    MatButtonModule,

  ],
  providers: [
    SiteService,
    CommonService,
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' }

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
