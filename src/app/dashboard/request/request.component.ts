import { Component, OnInit } from '@angular/core';
import { SiteService } from 'src/app/backend/site.service';
import { NavigationExtras, Router } from '@angular/router';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss']
})
export class RequestComponent implements OnInit {

  constructor(private router:Router,private siteService:SiteService) { }
  dtTrigger:Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};

  ngOnInit(): void {
    /////////////////////////////////////////////////////////////////////////////////////////////
    this.list_factor()
    /////////////////////////////////////////////////////////////////////////////////////////////
  }
  /////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////////
  // Fetching Product List // Products
  message:any = new Object();
  Factors:any = [];
  list_factor(){
    this.siteService.category_list().subscribe( // Note **
      resp => {
        this.Factors = resp
        console.log(resp);
        this.dtTrigger.next();
      })
    }

}
