import { DashboardHomeComponent } from './dashboard/dashboard-home/dashboard-home.component';
import { AuthGuard } from './backend/auth.guard';
import { SiteManagementComponent } from './aramb/site-management/site-management.component';
import { GalleryManagementComponent } from './aramb/gallery-management/gallery-management.component';
import { CustomerManagementComponent } from './aramb/customer-management/customer-management.component';
import { SubCategoryManagementComponent } from './aramb/sub-category-management/sub-category-management.component';
import { ArambHomeComponent } from './aramb/aramb-home/aramb-home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { DashboardNavComponent } from './dashboard/dashboard-nav/dashboard-nav.component';
import { CategoryManagementComponent } from './aramb/category-management/category-management.component';
import { AppointmentsManagementComponent } from './aramb/appointments-management/appointments-management.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full', },

  // Auth
  { path: 'signin', component: LoginComponent },
  // { path:'signup', component: RegisterComponent },


  // Admin-Panel
  {
    path: 'dashboard',
    component: DashboardNavComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: DashboardHomeComponent },
      // { path: '', component: ArambHomeComponent },
      // { path: 'sub-category-management', component: SubCategoryManagementComponent },
      { path: 'category-management', component: CategoryManagementComponent },

      { path: 'customer-management', component: CustomerManagementComponent },

      { path: 'appointments-management', component: AppointmentsManagementComponent },
      { path: 'gallery-management', component: GalleryManagementComponent },

      { path: 'site-management', component: SiteManagementComponent },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
